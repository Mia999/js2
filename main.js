while (true) {

  let name = prompt("What is your name?")

  console.log(name);
  console.log(typeof name);

  if (name === "" || name === null) {
    alert("Try again")
  } else if (!isNaN(name)) {
    alert("Try again. What is your name?")
  } else {

    let userAge = prompt("How old are you?")

    if (userAge === "" || userAge === null || isNaN(+userAge)) {
      alert("Try again. How old are you?")
    }

    if (userAge < 18) {
      alert("You are not allowed to visit this website.");
    }
    else if (userAge >= 18 && userAge <= 22) {
      let confirmation = confirm("Are you sure you want to continue?");
      if (confirmation === false) {
        alert("Try again")

      } else {
        alert("welcome" + ", " + name + "!")
        break
      }
    }

    if (userAge > 22) {
      alert(`Welcome, ${name}!`);
      break
    }
  }
}

